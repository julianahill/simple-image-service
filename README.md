# Image Serve
Simple tutorial on how to use service workers with Ruby on Rails. The purpose
of this application is to upload and store images to a server. This tutorial
assumes you know how to [Git](https://git-scm.com/), and are familiar with Unix.

## Specs:
* Ruby v2.4.0
* Rails v5.1.5
* SQLite3

## Gems:
* [serviceworker-rails](https://github.com/rossta/serviceworker-rails)
  * [overview](https://rossta.net/blog/service-worker-on-rails.html)
* [carrierwave](https://github.com/carrierwaveuploader/carrierwave)

## Guide
These steps are taken from a couple of places:
[rails tutorial](http://guides.railsgirls.com/app),
[serviceworker](https://github.com/rossta/serviceworker-rails).

### Step 1
You want to create a new app called `image-serve` by running:
```
$ rails new image-serve
$ cd image-serve
$ git init
```

### Step 2
Let's create a scaffold for data to be collected and stored. In the terminal, run:
```
$ rails generate scaffold idea name:string description:text picture:string
```
Next, run:
```
$ rails db:migrate
$ rails s
```

### Step 3
Open `app/views/layouts/application.html.erb` in your text editor and above the line
```
<%= stylesheet_link_tag    'application', media: 'all', 'data-turbolinks-track': 'reload' %>
```
add
```
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.css">
```
and replace
```
<%= yield %>
```
with
```
<div class="container">
  <%= yield %>
</div>
```
Let’s also add a navigation bar and footer to the layout.
In the same file, under `<body>` add
```
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/ideas/">The Idea app</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/ideas/">Ideas</a></li>
      </ul>
    </div>
  </div>
</nav>
```
and before `</body>` add
```
<footer>
  <div class="container">
    Image Serve <%= Time.now.year %>
  </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.js"></script>
```
Now let’s also change the styling of the ideas table.
Open `app/assets/stylesheets/application.css` and at the bottom add
```
body { padding-top: 100px; }
footer { margin-top: 100px; }
table, td, th { vertical-align: middle; border: none; }
th { border-bottom: 1px solid #DDD; }
```
Now make sure you saved your files and refresh the browser to see what was
changed. You can also change the HTML & CSS further.

### Step 4
Open `Gemfile` in the project directory using your text editor and under the line
```
gem 'sqlite3'
```
add
```
gem 'carrierwave'
```
In the terminal run:
```
$ bundle
```
Now we can generate the code for handling uploads. In the terminal, run:
```
$ rails generate uploader Picture
```
If an error is shown that the uploader cannot be found, also, add the following line:
```
gem 'net-ssh'
```
If you added this gem, please run in your terminal again:
```
$ bundle
```
Open `app/models/idea.rb`, and, under the line
```
class Idea < ApplicationRecord
```
add
```
mount_uploader :picture, PictureUploader
```
Open `app/views/ideas/_form.html.erb` and change
```
<%= form.text_field :picture, id: :idea_picture  %>
```
to
```
<%= form.file_field :picture, id: :idea_picture %>
```

Open `app/views/ideas/show.html.erb` and change
```
<%= @idea.picture %>
```
to
```
<%= image_tag(@idea.picture_url, width: 600) if @idea.picture.present? %>
```

### Step 5
Open http://localhost:3000 (or your preview url, if you are using a cloud service).
It still shows the `Yay! You’re on Rails!` page. Let’s make it redirect to the ideas page.

Open `config/routes.rb` and after the first line add
```
root to: redirect('/ideas/')
```
Test the change by opening the root path (that is, http://localhost:3000/ or
your preview url) in your browser.

Don't forget to change the pathing in `new.html.erb`, `show.html.erb`, and `edit.html.erb` from
```
<%= link_to 'Back', ideas_path %>
```
to
```
<%= link_to 'Back', '/ideas/' %>
```

In `ideas_controller.rb`, change `destroy` to
```
def destroy
  @idea.destroy
  respond_to do |format|
    format.html { redirect_to '/ideas/', notice: 'Idea was successfully destroyed.' }
    format.json { head :no_content }
  end
end
```

### Step 6
Add this line to your application's `Gemfile`:
```
gem 'serviceworker-rails'
```
And then execute:
```
$ bundle
```
After bundling the gem in your Rails project, run the generator from the root of your Rails project.
```
$ rails g serviceworker:install
```
The generator will create the following files:
* `config/initializers/serviceworker.rb` - for configuring your Rails app
* `app/assets/javascripts/serviceworker.js.erb` - a blank Service Worker script with some example strategies
* `app/assets/javascripts/serviceworker-companion.js` - a snippet of JavaScript necessary to register your Service Worker in the browser
* `app/assets/javascripts/manifest.json.erb` - a starter web app manifest pointing to some default app icons provided by the gem
* `public/offline.html` - a starter offline page

It will also make the following modifications to existing files:
* Adds a sprockets directive to `application.js` to require `serviceworker-companion.js`
* Adds `serviceworker.js` and `manifest.json` to the list of compiled assets in `config/initializers/assets.rb`
* Injects tags into the head of `app/views/layouts/application.html.erb` for linking to the web app manifest

### Step 7

We're going to add some real functionality to this code.
Right now, no connection returns an offline screen instead of a cached response.

Open `serviceworker-companion.js` and change the file to says:
```
if (navigator.serviceWorker) {
  navigator.serviceWorker.register('/serviceworker.js', { scope: '/ideas/' })
    .then((reg) => {
      console.log('[Companion]', 'Service worker registered!');
    });
}
```

Then, open up the `serviceworker.js.erb`, which contains something like:
```
var CACHE_VERSION = 'v1';
var CACHE_NAME = CACHE_VERSION + ':sw-cache-';

function onInstall(event) {
  console.log('[Serviceworker]', "Installing!", event);
  event.waitUntil(
    caches.open(CACHE_NAME).then(function prefill(cache) {
      return cache.addAll([

        // make sure serviceworker.js is not required by application.js
        // if you want to reference application.js from here
        '<%#= asset_path "application.js" %>',

        '<%= asset_path "application.css" %>',

        '/offline.html',

      ]);
    })
  );
}

function onActivate(event) {
  console.log('[Serviceworker]', "Activating!", event);
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.filter(function(cacheName) {
          // Return true if you want to remove this cache,
          // but remember that caches are shared across
          // the whole origin
          return cacheName.indexOf(CACHE_VERSION) !== 0;
        }).map(function(cacheName) {
          return caches.delete(cacheName);
        })
      );
    })
  );
}

// Borrowed from https://github.com/TalAter/UpUp
function onFetch(event) {
  event.respondWith(
    // try to return untouched request from network first
    fetch(event.request).catch(function() {
      // if it fails, try to return request from the cache
      return caches.match(event.request).then(function(response) {
        if (response) {
          return response;
        }
        // if not found in cache, return default offline content for navigate requests
        if (event.request.mode === 'navigate' ||
          (event.request.method === 'GET' && event.request.headers.get('accept').includes('text/html'))) {
          console.log('[Serviceworker]', "Fetching offline content", event);
          return caches.match('/offline.html');
        }
      })
    })
  );
}

self.addEventListener('install', onInstall);
self.addEventListener('activate', onActivate);
self.addEventListener('fetch', onFetch);
```

These functions are important to the service worker.
They are involved in each part of the service worker's life cycle.
To learn more about its life cycle, [click here](https://developers.google.com/web/fundamentals/primers/service-workers/).

In `onInstall(event)`, append `'/'` to the list of urls to be cached upon the service worker's installation. So, The function should read:
```
function onInstall(event) {
  console.log('[Serviceworker]', "Installing!", event);
  event.waitUntil(
    caches.open(CACHE_NAME).then(function prefill(cache) {
      return cache.addAll([
        // make sure serviceworker.js is not required by application.js
        // if you want to reference application.js from here
        '<%#= asset_path "application.js" %>',
        '<%= asset_path "application.css" %>',
        '/offline.html',
        '/',
        '/ideas/',
        '/ideas/new'
      ]);
    })
  );
}
```

Now, refresh your page, and you should get some console logs  about the service worker installing and activating.
![console](app/assets/images/ConsoleLog.png)

In that same console log window, tab over to `Application`. Click on `Service Worker` as depicted below:
![service worker](app/assets/images/ServiceWorker.png)

There will be an `href` in orange that says `skipWaiting`. Click on it to get your latest version running.

Sever the connection to your rails application, and refresh the page.

You'll see that you no longer have an offline screen, but your index page!

### Step 8
Let's try to get some image caching going.

In `serviceworker.js.erb`, change the top lines so they say
```
var CACHE_VERSION = 'v1';
var CACHE_NAME = 'sw-cache-' + CACHE_VERSION;
var IMG_CACHE = 'sw-content-imgs';
```

In `onActivate(event)`, update
```
return cacheName.indexOf(CACHE_VERSION) !== 0;
```
to
```
return cacheName.indexOf(CACHE_VERSION) === -1;
```
Next, lets add some filtering to our `onFetch(event)` function.

Add
```
var url = new URL(event.request.url);

if (method == 'GET') {
  if (url.pathname.startsWith('/uploads/')) {
    event.respondWith(servePhoto(event.request, url));
    return;
  }
}
```
before
```
event.respondWith(
```
in `onFetch(event)`

After the closing brace of `onFetch(event)`, add
```
function servePhoto(request, url) {
  return caches.open(IMG_CACHE).then(function(cache) {
    return cache.match(url.pathname).then(function(response) {
      if (response) return response;

      return fetch(request).then(function(networkResponse) {
        cache.put(url.pathname, networkResponse.clone());
        return networkResponse;
      });
    });
  });
}
```

Save all of these edits, run `rails s` in the terminal, and refresh your page.

Don't forget to `skipWaiting` in the application tab.

Navigate around the page, and you'll see the cache update with the new image.
![Caches](app/assets/images/Caches.png)

### Step 9

Now that we have images being cached, we can work on storing each
object's data into a local database. We're going to use `IndexedDB`.

Append
```
var DB_NAME = 'sw-db-' + CACHE_VERSION;

var idb = self.indexedDB;
if (idb) {
  var dbOpen = idb.open(DB_NAME, 1);

  dbOpen.onupgradeneeded = (event) => {
    var db = event.target.result;

    db.onerror = function(event) {
       console.log('Error loading database.');
    };

    db.createObjectStore('ideas', {keyPath: 'id'});
  };
}
```
to `line 4` of the service worker javascript file.

These lines will initialize your IndexedDB, so ideas may be stored.

Next, in `onFetch(event)` add
```
else if (url.pathname.match(/\/ideas\/[0-9]+/)) {
  storeJSON(url);
}
```
to
```
if (url.origin === location.origin) {
  if (url.pathname.startsWith('/uploads/')) {
    event.respondWith(servePhoto(event.request, url));
    return;
  }
}
```

After `servePhoto(request, url)`, add
```
function storeJSON(url) {
  //check for support
  if (!idb) {
    console.log('This browser doesn\'t support IndexedDB');
    return;
  }

  // setup request to get JSON
  var jsonRequest = new Request(url + '.json');

  // fetch the JSON
  fetch(jsonRequest).then((response) => {
    response.json().then((data) => {
      var dbOpen = idb.open(DB_NAME, 1);
      // save the data
      dbOpen.onsuccess = (event) => {
        console.log('Storing', data);
        var db = dbOpen.result;
        var tx = db.transaction('ideas', 'readwrite');
        tx.objectStore('ideas').put(data);
      };
    });
  }).catch(() => {
    console.log('Data not indexed due to network failure.');
    return caches.match('/offline.html');
  });
}
```

Now, refresh your browser, and update your service worker

Try adding an idea to the ideas table.
Navigate to the stored idea.

Check your `IndexedDB` found in the `Application` tab.
![IndexedDB](app/assets/images/IndexedDB.png)

### Step 10
Let's get the Show web page cached.

After `storeJSON(url)`, add
```
function serveShow(request, url) {
  storeJSON(url);
  return caches.open(CACHE_NAME).then((cache) => {
    return cache.match(url.pathname).then((response) => {
      if (response) return response;
      return fetch(request).then((networkResponse) => {
        cache.put(url.pathname, networkResponse.clone());
        return networkResponse;
      });
    });
  });
}
```
Then, in `onFetch(event)`, change
```
else if (url.pathname.match(/\/ideas\/[0-9]+/)) {
  storeJSON(url);
}
```
to
```
else if (url.pathname.match(/\/ideas\/[0-9]+/)) {
 event.respondWith(serveShow(event.request, url));
 return;
}
```
Refresh, and update your service worker.

### Step 11
Let's serve the edit page for each item.

In `onFetch(event)`, add
```
var method = event.request.method;
```
to the top of the function.

Change the if block to say
```
if (method == 'GET') {
  if (url.pathname.startsWith('/uploads/')) {
    event.respondWith(servePhoto(event.request, url));
    return;
  } else if (url.pathname.match(/\/ideas\/[0-9]+\/edit/)) {
    event.respondWith(serveEdit(event.request, url));
    return;
  } else if (url.pathname.match(/\/ideas\/[0-9]+/)) {
    event.respondWith(serveShow(event.request, url));
    return;
  }
}
```

Next, add this function to the file
```
function serveEdit(request, url) {
  // open caches
  return caches.open(CACHE_NAME).then((cache) => {
    // find a match to url path
    return cache.match(url.pathname).then((response) => {
      // if it's found return it
      if (response) return response;
      // else fetch the image to be cached and served
      return fetch(request).then((networkResponse) => {
        cache.put(url.pathname, networkResponse.clone());
        return networkResponse;
      });
    });
  });
}
```
Update your service worker.

### Step 12
Let's try to enqueue some requests to be sent once we get back online using `IndexedDB`.

To `dbOpen.onupgradeneeded`, add
```
db.createObjectStore('requests', {keyPath: 'timestamp'});
```

Next, to `onFetch(event)` add
```
switch (method) {
  case 'GET':
    if (url.pathname.startsWith('/uploads/')) {
      event.respondWith(servePhoto(event.request, url));
      return;
    } else if (url.pathname.match(/\/ideas\/[0-9]+\/edit/)) {
      event.respondWith(serveEdit(event.request, url));
      return;
    } else if (url.pathname.match(/\/ideas\/[0-9]+/)) {
      event.respondWith(serveShow(event.request, url));
      return;
    } break;
  case 'POST':
  case 'PUT':
  case 'DELETE':
    event.respondWith(requests(event.request));
    return;
}
```
and remove
```
if (method == 'GET') {
  if (url.pathname.startsWith('/uploads/')) {
    event.respondWith(servePhoto(event.request, url));
    return;
  } else if (url.pathname.match(/\/ideas\/[0-9]+\/edit/)) {
    event.respondWith(serveEdit(event.request, url));
    return;
  } else if (url.pathname.match(/\/ideas\/[0-9]+/)) {
    event.respondWith(serveShow(event.request, url));
    return;
  }
}
```
Then, add `requests()`, `storeRequest()` and `serialize()` to the service worker
```
function serialize(request) {
  var headers = {};
  for (var pair of request.headers.entries()) {
    headers[pair[0]] = pair[1];
  }

  var req = {
    headers: headers,
    url: request.url,
    method: request.method,
    //mode: request.mode,
    credentials: request.credentials,
    cache: request.cache,
    redirect: request.redirect,
    referrer: request.referrer
  };

  var data = {
    timestamp: (new Date()).toString(),
    request: req
  };

  if (request.referrer.indexOf('new') != -1 ||
      request.referrer.indexOf('edit') != -1) {
    return request.clone().formData().then(function(b) {
      var body = {};
      for (let e of b.entries()) body[e[0]] = e[1];
      data.request.body = body;
      return Promise.resolve(data);
    });
  } else {
    return request.clone().text().then(function(body) {
      data.request.body = body;
      return Promise.resolve(data);
    });
  }
}

function storeRequest(request) {
  return serialize(request).then(function(data) {
    var dbOpen = idb.open(DB_NAME, 1);
    // save the data
    dbOpen.onsuccess = (event) => {

      console.log('Storing', data);

      var db = dbOpen.result;
      var tx = db.transaction('requests', 'readwrite');
      tx.objectStore('requests').put(data);

      console.log('Request was indexed due to a network failure.');
    };
  }).then(() => {
    return caches.match('/ideas/');
  });
}

function requests(request) {
  if (navigator.onLine)
    return fetch(request).then((response) => {
      return response;
    }).catch((e) => {
      return caches.match('/offline.html');
    });

  return storeRequest(request);
}
```

### Step 13
Let's try to dequeue some requests to be sent once we get back online using `IndexedDB`.

```
function dequeue() {
  var dbOpen = idb.open(DB_NAME, 1);
  // save the data
  dbOpen.onsuccess = (event) => {
    var db = dbOpen.result;
    var tx = db.transaction('requests', 'readwrite');
    var request = tx.objectStore('requests').getAll();

    request.onsuccess = function(event) {
      var requests = event.target.result;
      sendInOrder(requests, dbOpen);
    };
  };
}

function sendInOrder(requests, dbOpen) {
  for (let r of requests) {
    var request = deserialize(r.request);

    if(navigator.onLine)
      fetch(request).then((response) => {

        var tx = dbOpen.result.transaction('requests', 'readwrite')
        tx.objectStore('requests').delete(r.timestamp);

        console.log('Deleted', r);

      }).catch((e) => { console.log(e) });
  }
}

function deserialize(data) {
  var url = new URL(data.url);

  var init = {
    headers: data.headers,
    method: data.method,
    //mode: data.mode,
    credentials: data.credentials,
    cache: data.cache,
    redirect: data.redirect,
    referrer: data.referrer,
    body: data.body
  }

  return new Request(url.pathname, init);
}
```
Add this to the bottom of the `serviceworker.js.erb`
```
self.addEventListener('online', dequeue);
```

### To be continued...
