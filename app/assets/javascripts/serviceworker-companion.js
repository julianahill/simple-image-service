if (navigator.serviceWorker) {
  navigator.serviceWorker.register('/serviceworker.js', { scope: '/ideas/' })
    .then((reg) => {
      console.log('[Companion]', 'Service worker registered!');
    });
}
